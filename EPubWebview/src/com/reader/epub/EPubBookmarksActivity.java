package com.reader.epub;

import java.util.ArrayList;

import com.reader.epub.db.EPubBookmark;
import com.reader.epub.db.EPubDataSource;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class EPubBookmarksActivity extends ListActivity implements OnItemClickListener {

	private ListView mList;
	
	private ArrayList<EPubBookmark> bookmarks;
	private EPubDataSource source;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
                                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        source = new EPubDataSource(this);
        try {
			source.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
        
        bookmarks = (ArrayList<EPubBookmark>) source.getAllBookmark();
        int n = bookmarks.size();
        
        ArrayList<String> bmarks = new ArrayList<String>();
        for (int i=0; i<n; i++) {
        	bmarks.add(bookmarks.get(i).getBook() + " -- Page " + bookmarks.get(i).getPage());
        }
        
		mList = getListView();
		mList.setAdapter(new ArrayAdapter<String> (this, R.layout.toc_item, bmarks));
		mList.setOnItemClickListener(this);
		
		registerForContextMenu(getListView());
	}
	
	@Override
	protected void onDestroy() {
		source.close();
		super.onDestroy();
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.menu_delete, menu);
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		int id = item.getItemId();
		
		switch (id) {
		case R.id.itemDelete:
			int position = item.getOrder();
			source.deleteBookmark(bookmarks.get(position).getId());
			
			ArrayAdapter<String> adapter = (ArrayAdapter<String>) getListView().getAdapter();
			adapter.remove(adapter.getItem(position));
			adapter.notifyDataSetChanged();
			
			return true;
		}
		
		return super.onContextItemSelected(item);
	}
	
	@Override
	public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
		Intent data = new Intent();
		data.putExtra("page", bookmarks.get(position).getPage()-1);
		setResult(RESULT_OK, data);
		finish();
	}
	
	@Override
	public void onBackPressed() {
		setResult(RESULT_CANCELED);
		finish();
		super.onBackPressed();
	}
}
