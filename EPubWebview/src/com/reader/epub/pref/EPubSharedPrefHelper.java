package com.reader.epub.pref;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;

public class EPubSharedPrefHelper {

	private SharedPreferences mPrefs;
	
	public EPubSharedPrefHelper(Context context) {
		mPrefs = context.getSharedPreferences("epub_data", Context.MODE_PRIVATE);
	}
	
	public void setRecentPage(String book, int page) {
		mPrefs.edit().putInt("lastpage-"+book, page).commit();
	}
	
	public int getRecentPage(String book) {
		return mPrefs.getInt("lastpage-"+book, 0);
	}
	
	public void setBackground(int color) {
		mPrefs.edit().putInt("bg-color", color).commit();
	}
	
	public int getBackground() {
		return mPrefs.getInt("bg-color", Color.WHITE);
	}
	
	public void setTextColor(int color) {
		mPrefs.edit().putInt("text-color", color).commit();
	}
	
	public int getTextColor() {
		return mPrefs.getInt("text-color", Color.BLACK);
	}
	
	public void setTextFont(String font) {
		mPrefs.edit().putString("text-style", font).commit();
	}
	
	public String getTextFont() {
		return mPrefs.getString("text-style", "initial");
	}
	
	public void setTextSize(int size) {
		mPrefs.edit().putInt("text-size", size).commit();
	}
	
	public int getTextSize() {
		return mPrefs.getInt("text-size", 50);
	}
	
	public void setTextMargin(String margin) {
		mPrefs.edit().putString("text-margin", margin).commit();
	}
	
	public String getTextMargin() {
		return mPrefs.getString("text-margin", "initial");
	}
	
	public void setLineSpacing(int spacing) {
		mPrefs.edit().putInt("line-spacing", spacing).commit();
	}
	
	public int getLineSpacing() {
		return mPrefs.getInt("line-spacing", 50);
	}
	
	public void setBackgroundNight(int color) {
		mPrefs.edit().putInt("bg-color-night", color).commit();
	}
	
	public int getBackgroundNight() {
		return mPrefs.getInt("bg-color-night", Color.BLACK);
	}
	
	public void setTextColorNight(int color) {
		mPrefs.edit().putInt("text-color", color).commit();
	}
	
	public int getTextColorNight() {
		return mPrefs.getInt("text-color", Color.WHITE);
	}
	
	public void setNightMode(boolean night) {
		mPrefs.edit().putBoolean("night-mode", night).commit();
	}
	
	public boolean isNightMode() {
		return mPrefs.getBoolean("night-mode", false);
	}
}