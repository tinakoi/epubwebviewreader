package com.reader.epub.widget;

public interface OnWebViewSwipeListener {

	public void onSwipeLeft();
	
	public void onSwipeRight();
}
