package com.reader.epub.widget;

import net.londatiga.android.ActionItem;
import net.londatiga.android.QuickAction;
import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.brandontate.androidwebviewselection.BTWebView;
import com.reader.epub.EPubActivity;
import com.reader.epub.R;

public class EPWebView extends BTWebView {

	private String mJSSCript;

	private OnSwipeTouchListener swipeListener;
	private OnWebViewSwipeListener webSwipeListener;
	
	private EPubActivity mActivity;
	
	public EPWebView(Context context, AttributeSet attrs) {
		super(context, attrs);
		swipeListener = new OnSwipeTouchListener(context) {
			
			@Override
			public void onSwipeLeft() {
				if (webSwipeListener != null) {
					webSwipeListener.onSwipeLeft();
				}
			}
			
			@Override
			public void onSwipeRight() {
				if (webSwipeListener != null) {
					webSwipeListener.onSwipeRight();
				}
			}
		};
	}
	
	public void setEPubActivity(EPubActivity activity) {
		this.mActivity = activity;
	}
	
	@Override
	public void showContextMenu(Rect displayRect) {

        // Don't show this twice
        if(mContextMenuVisible){
            return;
        }

        // Don't use empty rect
        //if(displayRect.isEmpty()){
        if(displayRect.right <= displayRect.left){
            return;
        }

        //Copy action item
        ActionItem buttonOne = new ActionItem();

        buttonOne.setTitle("Copy");
        buttonOne.setActionId(1);
        
        //Highlight action item
        ActionItem buttonTwo = new ActionItem();

        buttonTwo.setTitle("Google");
        buttonTwo.setActionId(2);
        
        ActionItem buttonThree = new ActionItem();

        buttonThree.setTitle("Wikipedia");
        buttonThree.setActionId(3);
        
        ActionItem buttonFour = new ActionItem();
        
        buttonFour.setTitle("Dictionary");
        buttonFour.setActionId(4);
        
        // The action menu
        mContextMenu  = new QuickAction(getContext());
        mContextMenu.setOnDismissListener(this);

        // Add buttons
        mContextMenu.addActionItem(buttonOne);
        mContextMenu.addActionItem(buttonTwo);
        mContextMenu.addActionItem(buttonThree);
        mContextMenu.addActionItem(buttonFour);



        //setup the action item click listener
        mContextMenu.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {

            @Override
            public void onItemClick(QuickAction source, int pos,
                                    int actionId) {
                // TODO Auto-generated method stub
            	if (actionId == 1) {
            		mActivity.copyToClipboard(getSelectedText());
            		endSelectionMode();
            } else if (actionId == 2) {
                    // Do Button 1 stuff
                	mActivity.searchGoogle(getSelectedText());
                	endSelectionMode();
                }
                else if (actionId == 3) {
                    // Do Button 2 stuff
                	mActivity.searchWiki(getSelectedText());
                	endSelectionMode();
                }
                else if (actionId == 4) {
                    // Do Button 3 stuff
                	mActivity.searchDictionary(getSelectedText());
                	endSelectionMode();
                }

                mContextMenuVisible = false;
                
            }

        });

        mContextMenuVisible = true;
        mContextMenu.show(this, displayRect);
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		swipeListener.onTouch(v, event);
		return super.onTouch(v, event);
	}
	
	public void setWebSwipeListener(OnWebViewSwipeListener listener) {
		this.webSwipeListener = listener;
	}
	
	/**
	 * API 14 already has a function canScrollHorizontally defined, we just added this
	 * to support devices before API 14.
	 */
	public boolean canScrollHorizontal(int direction) {
        final int offset = computeHorizontalScrollOffset();
        final int range = computeHorizontalScrollRange() - computeHorizontalScrollExtent();
        if (range == 0) 
        	return false;
        if (direction < 0) {
            return offset > 0;
        } else {
            return offset < range - 1;
        }
    }
	
	@Override
	public void loadDataWithBaseURL(String baseUrl, String data,
			String mimeType, String encoding, String historyUrl) {
		
		if (mJSSCript == null) {
			mJSSCript = "<script src='file:///android_asset/jquery.js'></script>" +
			"<script src='file:///android_asset/rangy-core.js'></script>" +
			"<script src='file:///android_asset/rangy-serializer.js'></script>" +
			"<script src='file:///android_asset/android.selection.js'></script>";
		}
		
		data = data.replace("<head>", "<head>" + mJSSCript);
				
//		loadUrl("file:///android_asset/jquery.js");
		super.loadDataWithBaseURL(baseUrl, data, mimeType, encoding, historyUrl);
	}
	
	public String getSelectedText() {
		return this.mSelectedText;
	}
}
