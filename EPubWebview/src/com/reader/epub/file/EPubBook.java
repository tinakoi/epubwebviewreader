package com.reader.epub.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

import nl.siegmann.epublib.domain.Book;
import nl.siegmann.epublib.domain.Resource;
import nl.siegmann.epublib.domain.TOCReference;
import nl.siegmann.epublib.domain.TableOfContents;
import nl.siegmann.epublib.epub.EpubReader;
import android.os.Environment;
import android.util.Log;

public class EPubBook {

	private Book mBook;

	private File mBookDirectory;

	private Map<String, String> mBaseUrl;
	
	private List<Resource> mContents;
	
	private int mPageNumber;
	
	private ArrayList<String> tocHrefs;
	private ArrayList<String> tocTitles;
	
	public EPubBook(File file) {
		
		try {
			mBook = new EpubReader().readEpub(new FileInputStream(file));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		unzip(file);
		
		prepareBaseUrls();
		
		mContents = mBook.getContents();
		
		prepareTOC();
	}

	private void prepareTOC() {
		List<TOCReference> refs = toc();
		int n = refs.size();
		
		tocHrefs = new ArrayList<String>();
		tocTitles = new ArrayList<String>();
		
		for (int i=0; i<n; i++) {
			TOCReference toc = refs.get(i);
			tocHrefs.add(toc.getResource().getHref());
			tocTitles.add(toc.getTitle());
		}
	}

	private void unzip(File file) {
		String path = Environment.getExternalStorageDirectory().getPath();

		// create directory
		File topDir = new File(path + "/epub/");
		if (!topDir.exists()) {
			topDir.mkdir();
		} else if (!topDir.isDirectory()) {
			topDir.mkdir();
		}
		
		// create directory for the book
		mBookDirectory = new File(path + "/epub/" + file.getName());
		if (!mBookDirectory.exists()) {
			mBookDirectory.mkdir();
		} else if (!mBookDirectory.isDirectory()) {
			mBookDirectory.mkdir();
		}
		
		ZipFile zip = null;
		
		// unzip file to the directory
		try {
			zip = new ZipFile(file, ZipFile.OPEN_READ);
			Enumeration<?> entries = zip.entries();
			
			while (entries.hasMoreElements()) {
				ZipEntry entry = (ZipEntry) entries.nextElement();
				
				mkdir(mBookDirectory, entry.getName());
				
				if (entry.isDirectory()) {
					File entryFile = new File(mBookDirectory, entry.getName());
					entryFile.mkdir();
				} else {
					File entryFile = new File(mBookDirectory, entry.getName());
					FileOutputStream fos = new FileOutputStream(entryFile);
					InputStream is = zip.getInputStream(entry);
					byte[] buffer = new byte[2048];
					int bytesRead = 0;
					while ((bytesRead = is.read(buffer)) != -1) {
	                    fos.write(buffer, 0, bytesRead);
	                }
	                fos.close();
				}
			}
			
			zip.close();
		} catch (ZipException e) {
			e.printStackTrace();
			Log.w("test", e.toString());
		} catch (IOException e) {
			e.printStackTrace();
			Log.w("test", e.toString());
		}
	}

	private void mkdir(File root, String entry) {
		String[] splits = entry.split("/");
		int len = splits.length;
		
		if (len <= 1) {
			return;
		}
		
		String addon = "";
		
		for (int i=0; i<len-1; i++) {
			File temp = new File(root + addon, splits[i]);
			temp.mkdir();
			addon = "/" + splits[i];
		}
	}
	
	public void delete() {
		if (mBookDirectory != null && mBookDirectory.exists()) {
			deleteFile(mBookDirectory);
		}
	}

	private void deleteFile(File file) {
		if (file != null && file.exists()) {
			if (file.isDirectory()) {
				File[] files = file.listFiles();
				int size = files.length;
				for (int i=0; i<size; i++) {
					deleteFile(files[i]);
				}
			}
			file.delete();
		}
	}
    
	private void prepareBaseUrls() {
		mBaseUrl = new HashMap<String, String>();
    	
		getXHTMLFile(mBookDirectory);
	}

	private void getXHTMLFile(File file) {
		if (file.isDirectory()) {
			File[] files = file.listFiles();
			int length = files.length;
			for (int i=0; i<length; i++) {
				File temp = files[i];
				
				if (temp.isDirectory()) {
					getXHTMLFile(temp);
				} else {
					String path = temp.getAbsolutePath();
					String name = temp.getName();
					if (name.endsWith(".xhtml")) {
						String value = path.substring(0, path.lastIndexOf(name));
						mBaseUrl.put(name, value);
					}
				}
			}
		}
	}
	
	public String getTitle() {
		return mBook.getTitle();
	}
	
	public byte[] getPage() throws IOException {
		return mContents.get(mPageNumber).getData();
	}

	public String getBaseUrl() {
		String href = mContents.get(mPageNumber).getHref();
		String clean = href.substring(href.lastIndexOf("/")+1);
		
		return mBaseUrl.get(clean);
	}
	
	public int getTotalPages() {
		return mContents.size();
	}
	
	public void setPage(int page) {
		this.mPageNumber = page;
	}

	public int getCurrentPage() {
		return this.mPageNumber + 1;
	}
	
	public void listContent() {
		int n = mContents.size();
		
		for (int i=0; i<n; i++) {
			String page = mContents.get(i).getHref();
			Log.i("test", "Page #" + (i+1) + " - " + page);
		}
	}
	
	public boolean nextPage() {
		int total = getTotalPages();
		
		if (mPageNumber < total-1) {
			mPageNumber++;
			return true;
		}
		
		return false;
	}
	
	public boolean previousPage() {
		if (mPageNumber > 0) {
			mPageNumber--;
			return true;
		}
		
		return false;
	}
	
	public List<TOCReference> toc() {
		TableOfContents toc = mBook.getTableOfContents();
		
		return toc.getTocReferences();
	}
	
	public void displayTOC() {
		List<TOCReference> toc = toc();
		int n = toc.size();
		
		Log.i("toc", "TOC="+n);
		for (int i=0; i<n; i++) {
			TOCReference ref = toc.get(i);
			Log.i("toc", ref.getCompleteHref());
			Log.i("toc", ref.getTitle());
			Log.i("toc", ref.getResource().getHref());
		}
	}
	
	public ArrayList<String> getTOCHrefs() {
		return tocHrefs;
	}
	
	public ArrayList<String> getTOCTitles() {
		return tocTitles;
	}

	// Given a URL, find the page from the spine
	public void setPage(String uri) {
		int n = mContents.size();
		
		for (int i=0; i<n; i++) {
			String temp = mContents.get(i).getHref();
			if (uri.contains(temp) || temp.compareTo(uri) == 0) {
				mPageNumber = i;
				break;
			}
		}
	}
}
