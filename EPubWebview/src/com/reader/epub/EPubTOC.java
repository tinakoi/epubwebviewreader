package com.reader.epub;

import java.util.ArrayList;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class EPubTOC extends ListActivity implements OnItemClickListener {

	private ListView mList;
	
	private ArrayList<String> href;
	private ArrayList<String> title;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
                                WindowManager.LayoutParams.FLAG_FULLSCREEN);

		href = (ArrayList<String>) getIntent().getSerializableExtra("toc_ref");
		title = (ArrayList<String>) getIntent().getSerializableExtra("toc_titles");
		
		mList = getListView();
		mList.setAdapter(new ArrayAdapter<String> (this, R.layout.toc_item, title));
		mList.setOnItemClickListener(this);
	}
	
	@Override
	public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
		Intent data = new Intent();
		data.putExtra("go_href", href.get(position));
		data.putExtra("title", title.get(position));
		setResult(RESULT_OK, data);
		finish();
	}
	
	@Override
	public void onBackPressed() {
		setResult(RESULT_CANCELED);
		finish();
		super.onBackPressed();
	}
}
