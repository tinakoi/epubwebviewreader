package com.reader.epub;

import java.io.File;

import yuku.ambilwarna.AmbilWarnaDialog;
import yuku.ambilwarna.AmbilWarnaDialog.OnAmbilWarnaListener;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings.SettingNotFoundException;
import android.provider.Settings.System;
import android.text.ClipboardManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.reader.epub.db.EPubDataSource;
import com.reader.epub.file.EPubBook;
import com.reader.epub.pref.EPubSharedPrefHelper;
import com.reader.epub.widget.EPWebView;
import com.reader.epub.widget.OnWebViewSwipeListener;

public class EPubActivity extends Activity implements OnWebViewSwipeListener {
	
	public final static short SCREEN_BRIGHTNESS_MAX = 255;
	public final static short SCREEN_BRIGHTNESS_MIN = 20;
	public final static String KEY_BUNDLE_BRIGHTNESS = "EPubActivity.screenBrightness";
	
	private ViewFlipper mBookFlipper;
	private EPWebView mPage1, mPage2, mPage3;
	private TextView mPageNumber;
	private SeekBar mPageSlider;
	private ImageButton mToggleBookmark;
	
	private boolean isBookmarked;
	
	private Animation flipOut, flipIn, flipOutRev, flipInRev;
	
	private EPubBook mBook;
	
	private int forceOrienation;
	private int screenBrightness;
	
	private EPubDataSource mDataSource;

	private EPubSharedPrefHelper mPrefs;
	private Bundle mSearchBundle;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
                                WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.activity_epub);
		
		Uri uri = getIntent().getData();
		
		if (uri != null) {
			// open book here
			mBook = new EPubBook(new File(uri.getPath()));
		}
		
		mDataSource = new EPubDataSource(this);
		try {
			mDataSource.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		mPrefs = new EPubSharedPrefHelper(this);
		
		initAnimation();
		initLayout();
		
		setTitle(mBook.getTitle());
	}
	
	@Override
	protected void onDestroy() {
		mBook.delete();
		mBook = null;
		mDataSource.close();
		super.onDestroy();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		mBook.setPage(mPrefs.getRecentPage(mBook.getTitle()));
		updatePage();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.menu_epub, menu);
	    
	    updateMenuItem(menu);
	    
	    return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.screenUnspecified:
			processOrientationConfig(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
			return true;
		case R.id.screenLandscape:
			processOrientationConfig(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
			return true;
		case R.id.screenPortrait:
			processOrientationConfig(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
			return true;
		case R.id.itemTOC:
			Intent intent = new Intent(this, EPubTOC.class);
			intent.putExtra("toc_ref", mBook.getTOCHrefs());
			intent.putExtra("toc_titles", mBook.getTOCTitles());
			startActivityForResult(intent, 1000);
			return true;
		case R.id.itemBookmarks:
			startActivityForResult(new Intent(this, EPubBookmarksActivity.class), 1001);
			return true;
		case R.id.addNote:
			showAddNote();
			return true;
		case R.id.listNotes:
			startActivityForResult(new Intent(this, EPubNotesActivity.class), 1002);
			return true;
		case R.id.itemBackground:
			changeBackgroundColor();
			return true;
		case R.id.changeFontColor:
			changeFontColor();
			return true;
		case R.id.itemViewMode:
			if (mPrefs.isNightMode()) {
				mPrefs.setNightMode(false);
				item.setTitle("Night Mode");
				updatePage();
			} else {
				mPrefs.setNightMode(true);
				item.setTitle("Day Mode");
				updatePage();
			}
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		switch (requestCode) {
		case 1000:
			if (resultCode == RESULT_OK && data != null) {
//				getActiveWebView().load
				String title = data.getStringExtra("title");
				String href = data.getStringExtra("go_href");
				
				mBook.setPage(href);
				mPrefs.setRecentPage(mBook.getTitle(), mBook.getCurrentPage()-1);
				
				mSearchBundle = new Bundle();
				mSearchBundle.putString("search_key", title);
			}
			break;
		case 1001:
			if (resultCode == RESULT_OK && data != null) {
				int page = data.getIntExtra("page", mBook.getCurrentPage()-1);
				mPrefs.setRecentPage(mBook.getTitle(), page);
				mBook.setPage(page);
			}
			break;
		case 1002:
			if (resultCode == RESULT_OK && data != null) {
				int page = data.getIntExtra("page", mBook.getCurrentPage()-1);
				mPrefs.setRecentPage(mBook.getTitle(), page);
				mBook.setPage(page);
			}
			break;
		default:
			break;
		}
	}

	private void swipeRight() {
		if (!mBook.previousPage()) {
			return;
		}
		
		if (getActiveWebView().canScrollHorizontal(-1)) {
			return;
		}
		
		mBookFlipper.setInAnimation(flipInRev);
		mBookFlipper.setOutAnimation(flipOutRev);
		mBookFlipper.showPrevious();
		
		updatePage();
	}
	
	private void swipeLeft() {
		if (!mBook.nextPage()) {
			return;
		}
		
		if (getActiveWebView().canScrollHorizontal(1)) {
			return;
		}
		
		mBookFlipper.setInAnimation(flipIn);
		mBookFlipper.setOutAnimation(flipOut);
		mBookFlipper.showNext();

		updatePage();
	}
	
	private void initAnimation() {
		flipOut = AnimationUtils.loadAnimation(this, R.anim.flipout);
		flipIn = AnimationUtils.loadAnimation(this, R.anim.flipin);
		
		flipOutRev = AnimationUtils.loadAnimation(this, R.anim.flipout_reverse);
		flipInRev = AnimationUtils.loadAnimation(this, R.anim.flipin_reverse);
	}

	private void initLayout() {
		forceOrienation = -99;

		screenBrightness = SCREEN_BRIGHTNESS_MIN;
		SeekBar seekbar = (SeekBar)findViewById(R.id.brightSeeker);
		seekbar.setMax(SCREEN_BRIGHTNESS_MAX);
		seekbar.setKeyProgressIncrement(1);
		 
        try {
            //Get the current system brightness
        	screenBrightness = System.getInt(getContentResolver(), System.SCREEN_BRIGHTNESS);
        } catch (SettingNotFoundException e) {
            screenBrightness = SCREEN_BRIGHTNESS_MIN;
            adjustScreenBrightness();
        }
        seekbar.setProgress(screenBrightness);
		seekbar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			
			@Override
			public void onStopTrackingTouch(SeekBar arg0) {
				adjustScreenBrightness();
			}
			
			@Override
			public void onStartTrackingTouch(SeekBar arg0) {
				// Do nothing
			}
			
			@Override
			public void onProgressChanged(SeekBar seekbar, int progress, boolean fromUser) {
				if(progress <= SCREEN_BRIGHTNESS_MIN){
					screenBrightness = SCREEN_BRIGHTNESS_MIN;
				} else {
					screenBrightness = progress;
				}
			}
		});
		mBookFlipper = (ViewFlipper) findViewById(R.id.bookFlipper);
		
		mPage1 = (EPWebView) findViewById(R.id.bookPreviousPage);
		mPage2 = (EPWebView) findViewById(R.id.bookCurrentPage);
		mPage3 = (EPWebView) findViewById(R.id.bookNextPage);

		initWebView(mPage1);
		initWebView(mPage2);
		initWebView(mPage3);

		mPage1.setWebSwipeListener(this);
		mPage2.setWebSwipeListener(this);
		mPage3.setWebSwipeListener(this);
		
		mPage1.setEPubActivity(this);
		mPage2.setEPubActivity(this);
		mPage3.setEPubActivity(this);
		
		mPageNumber = (TextView) findViewById(R.id.pageNumber);
		displayPageNumber();
		
		mPageSlider = (SeekBar) findViewById(R.id.pageSlider);
		mPageSlider.setMax(mBook.getTotalPages()-1);
		mPageSlider.setProgress(mBook.getCurrentPage()-1);
		mPageSlider.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				
			}
			
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				
			}
			
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				if (fromUser) {
					onPageSlide(progress);
				}
			}
		});
		
		mToggleBookmark = (ImageButton) findViewById(R.id.toggleBookmark);
		mToggleBookmark.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (isBookmarked) {
					deleteBookmark();
					validateBookmark();
				} else {
					addBookmark();
					validateBookmark();
				}
			}
		});
	}
	
	@SuppressLint({ "SetJavaScriptEnabled", "NewApi" })
	private void initWebView(WebView webView) {
        webView.setWebViewClient(new ReaderWebClient());
        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setBuiltInZoomControls(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
        	webView.getSettings().setDisplayZoomControls(false);
        }
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setAllowFileAccess(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setLoadWithOverviewMode(false);
        webView.getSettings().setUserAgentString("Android");
        webView.setInitialScale(1);
	}

	//#####Change of Device Orientation
	public void processOrientationConfig(int newOrient){
		boolean shouldApplyForceOrient = checkForceOrientPossibility(newOrient);
		if(shouldApplyForceOrient){
			forceOrienation = newOrient;
			applyForceOrientation(newOrient);
		}
	}
	public void applyForceOrientation(int orientation){
		setRequestedOrientation (orientation);
	}
	public boolean checkForceOrientPossibility(int newOrient){
		if(newOrient != forceOrienation){
			return true;
		}
		return false;
	}
	//#####change of screen brightness
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt(KEY_BUNDLE_BRIGHTNESS, screenBrightness);
	}
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		screenBrightness = savedInstanceState.getInt(KEY_BUNDLE_BRIGHTNESS);
		adjustScreenBrightness();
	}
	public void adjustScreenBrightness(){
		WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.screenBrightness = screenBrightness/(SCREEN_BRIGHTNESS_MAX*1.0f);
        getWindow().setAttributes(lp);
	}
	
	public void onPageSlide(int progress) {
		mBook.setPage(progress);
		try {
			String data = new String(mBook.getPage());
			data = formatData(data);
			getActiveWebView().loadDataWithBaseURL(
					"file:///" + mBook.getBaseUrl(),
					data,
					"application/xhtml+xml",
					"utf-8",
					null
			);
		} catch (Exception e) {
			e.printStackTrace();
		}
		displayPageNumber();
	}
	
	public void displayPageNumber() {
		int totalPage = mBook.getTotalPages();
		int currentPage = mBook.getCurrentPage();
		
		mPageNumber.setText(currentPage + "/" + totalPage);
	}
	
	public void openInternalLink(String uri) {
		mBook.setPage(uri);
		updatePage();
	}
	
	// Open link to an external browser application
	public void openExternalLink(String uri) {
    	Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
    	startActivity(intent);
	}
	
	public void openEmailAddress(String uri) {
		startActivity(new Intent(android.content.Intent.ACTION_SEND, Uri.parse(uri)));
	}
	
	public void addBookmark() {
		mDataSource.createBookmark(mBook.getTitle(), mBook.getCurrentPage());
	}
	
	public void deleteBookmark() {
		mDataSource.deleteBookmark(mBook.getTitle(), mBook.getCurrentPage());
	}
	
	public void addNote(String note) {
		mDataSource.createNote(mBook.getTitle(), mBook.getCurrentPage(), note);
	}

	public void playVideo(String uri) {
		
	}
	
	public void playAudio(String uri) {
		
	}
	
	public void adjustBrightness() {
		
	}
	
	public void changeFontSize(int size) {
		
	}
	
	public void changeFontColor() {
//	     initialColor is the initially-selected color to be shown in the rectangle on the left of the arrow.
       //     for example, 0xff000000 is black, 0xff0000ff is blue. Please be aware of the initial 0xff which is the alpha.

		int defaultColor = mPrefs.isNightMode() ? mPrefs.getTextColorNight() : mPrefs.getTextColor();
		
       AmbilWarnaDialog dialog = new AmbilWarnaDialog(this, defaultColor, new OnAmbilWarnaListener() {

           // Executes, when user click Cancel button
           @Override
           public void onCancel(AmbilWarnaDialog dialog){
           }

           // Executes, when user click OK button
           @Override
           public void onOk(AmbilWarnaDialog dialog, int color) {
               mPrefs.setTextColor(color);
               updatePage();
           }
       });
       dialog.show();		
	}
	
	public void changeFontType() {
		
	}
	
	public void changeBackgroundColor() {
//	     initialColor is the initially-selected color to be shown in the rectangle on the left of the arrow.
        //     for example, 0xff000000 is black, 0xff0000ff is blue. Please be aware of the initial 0xff which is the alpha.
 
		int defaultColor = mPrefs.isNightMode() ? mPrefs.getBackgroundNight() : mPrefs.getBackground();
		
        AmbilWarnaDialog dialog = new AmbilWarnaDialog(this, defaultColor, new OnAmbilWarnaListener() {
 
            // Executes, when user click Cancel button
            @Override
            public void onCancel(AmbilWarnaDialog dialog){
            }
 
            // Executes, when user click OK button
            @Override
            public void onOk(AmbilWarnaDialog dialog, int color) {
            	if (mPrefs.isNightMode()) {
            		mPrefs.setBackgroundNight(color);
            	} else {
                	mPrefs.setBackground(color);
            	}
                updatePage();
            }
        });
        dialog.show();
	}
	
	public void changeMargin(int margin) {
		
	}
	
	public void changeLineSpacing(int spacing) {
		
	}
	
	public void copyToClipboard(String text) {
		ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE); 
		 clipboard.setText(text);
		 Toast.makeText(this, "Copied text to clipboard: " + text, Toast.LENGTH_SHORT).show();
	}
	
	public void highlightText(String text) {
		
	}
	
	// Search google using external device browser
	public void searchGoogle(String keyword) {
		Intent intent = new Intent(Intent.ACTION_WEB_SEARCH);
		intent.putExtra(SearchManager.QUERY, keyword);
		startActivity(intent);
	}
	
	// Search wikipedia using external device browser
	public void searchWiki(String keyword) {
		String url = "http://en.wikipedia.org/wiki/Special:Search?search=" + keyword;
    	Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
    	startActivity(intent);
	}
	
	// Search free dictionary using external device browser
	public void searchDictionary(String keyword) {
    	String url = "http://www.freedictionary.org/?Query=" + keyword;
    	Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
    	startActivity(intent);
	}
	
	// This only works in the current visible web page
	// TODO: include other pages
	@SuppressWarnings("deprecation")
	public void searchText(String keyword) {
		int index = mBookFlipper.getDisplayedChild();
		WebView wv = null;
		
		switch (index) {
		case 0:
			wv = mPage1;
			break;
		case 1:
			wv = mPage2;
			break;
		case 2:
			wv = mPage3;
			break;
		}
		
		if (wv != null) {
			wv.findAll(keyword);
		}
	}
	
	public void searchNext(boolean forward) {
		int index = mBookFlipper.getDisplayedChild();
		WebView wv = null;
		
		switch (index) {
		case 0:
			wv = mPage1;
			break;
		case 1:
			wv = mPage2;
			break;
		case 2:
			wv = mPage3;
			break;
		}

		
		if (wv != null) {
			wv.findNext(forward);
		}
	}
	
	private class ReaderWebClient extends WebViewClient {
		
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			if (url.startsWith("file:///")) {
				openInternalLink(url);
			} else if (url.startsWith("http://")) {
				openExternalLink(url);
			} else if (url.startsWith("mailto:")) {
				openEmailAddress(url);
			}
			return true;	
		}
		
		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			validateBookmark();
			mPrefs.setRecentPage(mBook.getTitle(), mBook.getCurrentPage()-1);
			super.onPageStarted(view, url, favicon);
		}
		
		@Override
		public void onPageFinished(WebView view, String url) {
			if (mSearchBundle != null) {
				/* Not Accurate But Working */
				view.findAll(mSearchBundle.getString("search_key"));
				view.clearMatches();
				mSearchBundle = null;
			}
			
			super.onPageFinished(view, url);
		}
	}
	
	public void updatePage() {
		WebView wv = getActiveWebView();
		wv.clearView();

		try {
			String data = new String(mBook.getPage());
			data = formatData(data);
			wv.loadDataWithBaseURL(
					"file:///" + mBook.getBaseUrl(),
					data,
					"application/xhtml+xml",
					"utf-8",
					null
			);
		} catch (Exception e) {
			e.printStackTrace();
		}
		mPageSlider.setProgress(mBook.getCurrentPage()-1);
		displayPageNumber();
	}
	
	private String formatData(String data) {
		
		int bg = mPrefs.isNightMode() ? mPrefs.getBackgroundNight() : mPrefs.getBackground();
		int text = mPrefs.isNightMode() ? mPrefs.getTextColorNight() : mPrefs.getTextColor();
		String style = "<body style=\"";
		
		// change bg color
		style += "background-color:" + rgbString(bg) + ";";
		
		// change text color
		style += "color:" + rgbString(text) + ";";
		
		// change text size
		style += "font-size:" + mPrefs.getTextSize() + "px;";
		
		// change line spacing
		style += "line-height:" + mPrefs.getLineSpacing() + "px;";
		
		// change margin
		style += "margin:" + mPrefs.getTextMargin() + ";";
		
		// change style
		style += "font-family:" + mPrefs.getTextFont() + ";";
		
		style += "\">";

		data = data.replace("<body>", style);
		
		return data;
	}
	
	private String rgbString(int color) {
		int r = Color.red(color);
		int g = Color.green(color);
		int b = Color.blue(color);
		
		return "rgb("+r+","+g+","+b+")";
	}

	public void validateBookmark() {
		isBookmarked = mDataSource.hasBookmark(mBook.getTitle(), mBook.getCurrentPage());
		
		mToggleBookmark.setImageResource(isBookmarked ? R.drawable.bookmark_on : R.drawable.bookmark_off);
	}

	public EPWebView getActiveWebView() {
		int child = mBookFlipper.getDisplayedChild();
		
		switch (child) {
		case 0: return mPage1;
		case 1: return mPage2;
		case 2: return mPage3;
		default: return null;
		}
	}
	
	public void showAddNote() {
		AlertDialog.Builder alert = new AlertDialog.Builder(this);

		alert.setTitle("Enter Note:");

		// Set an EditText view to get user input 
		final EditText input = new EditText(this);
		alert.setView(input);

		alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
		public void onClick(DialogInterface dialog, int whichButton) {
			String value = input.getText().toString();// Do something with value!
			addNote(value);
		  }
		});

		alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
		  public void onClick(DialogInterface dialog, int whichButton) {
		    // Canceled.
		  }
		});

		alert.show();
	}

	
	private void updateMenuItem(Menu menu) {
		MenuItem viewModeItem = menu.findItem(R.id.itemViewMode);
		
		if (mPrefs.isNightMode()) {
			viewModeItem.setTitle("Day Mode");
		} else {
			viewModeItem.setTitle("Night Mode");
		}
	}

	@Override
	public void onSwipeLeft() {
		swipeLeft();
	}

	@Override
	public void onSwipeRight() {
		swipeRight();
	}
}
