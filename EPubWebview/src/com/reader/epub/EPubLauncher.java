package com.reader.epub;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import android.app.Activity;
import android.content.Intent;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class EPubLauncher extends Activity {

	private File sample1;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		try {
			init();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		Button button = new Button(this);
		button.setText("Press Me!");
		button.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (!sample1.exists())
					return;
				Uri uri = Uri.fromFile(sample1);
				Intent epub = new Intent(EPubLauncher.this, EPubActivity.class);
				epub.setAction(Intent.ACTION_VIEW);
				epub.setData(uri);
				startActivity(epub);
			}
		});
		setContentView(button);
	}

	private void init() throws IOException {
		AssetManager am = getAssets();
		
		InputStream is = am.open("books/moby-dick-mo-20120214.epub");
		
		File root = Environment.getExternalStorageDirectory();
		sample1 = new File(root, "moby-dick-mo-20120214.epub");
		
		if (!sample1.exists()) {

			FileOutputStream fos = new FileOutputStream(sample1);
			byte[] buf = new byte[1024];
			int len;
			while ((len = is.read(buf)) > 0) {
				fos.write(buf, 0, len);
			}
			
			is.close();
			fos.close();
		}
	}
}
