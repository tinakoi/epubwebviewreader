package com.reader.epub.db;

import java.io.Serializable;

public class EPubNote implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7872873485923616221L;

	public int id;
	
	public String book;
	
	public int page;
	
	public String note;
	
	public EPubNote() {
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBook() {
		return book;
	}

	public void setBook(String book) {
		this.book = book;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}
	
	
}
