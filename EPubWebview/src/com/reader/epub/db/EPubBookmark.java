package com.reader.epub.db;

import java.io.Serializable;

public class EPubBookmark implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6573124987345361225L;

	public int id;
	
	public String book;
	
	public int page;
	
	public EPubBookmark() {
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBook() {
		return book;
	}

	public void setBook(String book) {
		this.book = book;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}
	
	
}
